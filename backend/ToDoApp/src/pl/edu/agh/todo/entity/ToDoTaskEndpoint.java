package pl.edu.agh.todo.entity;

import pl.edu.agh.todo.persistence.PMF;
import pl.edu.agh.todo.constants.Constants;
import pl.edu.agh.todo.entity.ToDoTask.TaskStatus;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.users.User;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(name = "todotaskendpoint", namespace = @ApiNamespace(ownerDomain = "edu.pl", ownerName = "edu.pl", packagePath = "agh.todo.entity"),  scopes = {Constants.EMAIL_SCOPE},
clientIds = {Constants.IOS_CLIENT_ID, Constants.WEB_CLIENT_ID, com.google.api.server.spi.Constant.API_EXPLORER_CLIENT_ID})
public class ToDoTaskEndpoint {

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listToDoTask")
	public CollectionResponse<ToDoTask> listToDoTask(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit, 
			@Nullable @Named("lastUpdate") Date lastUpdate,
			User user) throws UnauthorizedException {
		if (user == null) throw new UnauthorizedException("User is Not Valid");

		if (lastUpdate == null ) {
			lastUpdate =  new Date(0);
		}
		
		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<ToDoTask> execute = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(ToDoTask.class);
			query.setFilter("lastUpdate > dateParam && ownerId == '"+ user.getUserId() +"' && status == "+TaskStatus.NORMAL.getValue());
			query.declareParameters("java.util.Date dateParam");
			
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}

			execute = (List<ToDoTask>) query.execute(lastUpdate);
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (ToDoTask obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<ToDoTask> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 * @throws UnauthorizedException 
	 */
	@ApiMethod(name = "getToDoTask")
	public ToDoTask getToDoTask(@Named("id") Long id, User user) throws UnauthorizedException {
		if (user == null) throw new UnauthorizedException("User is Not Valid");
		PersistenceManager mgr = getPersistenceManager();
		ToDoTask todotask = null;
		try {
			todotask = mgr.getObjectById(ToDoTask.class, id);
		} finally {
			mgr.close();
		}
		return todotask;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param todotask the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertToDoTask")
	public ToDoTask insertToDoTask(ToDoTask todotask, User user) throws UnauthorizedException {
		if (user == null) throw new UnauthorizedException("User is Not Valid");
		PersistenceManager mgr = getPersistenceManager();
		
		todotask.setOwnerId(user.getUserId());
		todotask.setLastUpdate(new Date());
		todotask.setStatus(TaskStatus.NORMAL.getValue());
		
		try {
			if (todotask.getId() != null) {
				if (containsToDoTask(todotask)) {
					throw new EntityExistsException("Object already exists");
				}
			}
			mgr.makePersistent(todotask);
		} finally {
			mgr.close();
		}
		return todotask;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param todotask the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateToDoTask")
	public ToDoTask updateToDoTask(ToDoTask todotask, User user) throws UnauthorizedException {
		
		if (user == null){ 
			throw new UnauthorizedException("User is Not Valid");
		}
		
		todotask.setLastUpdate(new Date());
		
		if ( (todotask.getOwnerId() != null)){
			
			if (todotask.getOwnerId().equalsIgnoreCase("-1")){
				todotask.setStatus(TaskStatus.DELETED.getValue());
			}else {
				if (todotask.getOwnerId().equalsIgnoreCase("-2")){
					todotask.setStatus(TaskStatus.DONE.getValue());
				}
			}
			
		}
		todotask.setOwnerId(user.getUserId());
		
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsToDoTask(todotask)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(todotask);
		} finally {
			mgr.close();
		}
		return todotask;
	}

	private boolean containsToDoTask(ToDoTask todotask) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(ToDoTask.class, todotask.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}

}
