package pl.edu.agh.todo.constants;

public class Constants {
	  public static final String IOS_CLIENT_ID = "606981375120-g890je64k0c9ov7pog4s35bs1hnp3463.apps.googleusercontent.com";
	  public static final String WEB_CLIENT_ID = "606981375120-tf4vokgh7q8jn5t0ktsv52pqnkeieklh.apps.googleusercontent.com";
	  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
}