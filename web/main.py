from apiclient.discovery import build
from oauth2client.appengine import OAuth2Decorator
import jinja2
import webapp2
import os
from handlers import MainPageHandler, ApiHandler, decorator_api, decorator_app
application = webapp2.WSGIApplication([
    ('/', MainPageHandler),
    ('/api', ApiHandler),
    (decorator_app.callback_path, decorator_app.callback_handler()),
    (decorator_api.callback_path, decorator_api.callback_handler()),
])
