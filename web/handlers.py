from apiclient.discovery import build
from google.appengine.api import users
from oauth2client.appengine import OAuth2Decorator, CredentialsModelToDoApp
from oauth2client.client import AccessTokenRefreshError
import jinja2
import webapp2
import os
import jsonrpc
import json
import logging
import httplib2

decorator_app = OAuth2Decorator(
    client_id='188325848906-k067ms0hjudovssm2ruia6vi1tmjtpsk.apps.googleusercontent.com',
    client_secret='xXbyIWXll4b5KX52abTumZ5K',
    scope='profile',
    _credentials_class=CredentialsModelToDoApp,
    _credentials_property_name='app_credentials'
)

decorator_api = OAuth2Decorator(
    client_id='606981375120-tf4vokgh7q8jn5t0ktsv52pqnkeieklh.apps.googleusercontent.com',
    client_secret='DkCEjNwO_Pb3pr63a8xOtc3z',
    scope='email',
    callback_path='/oauth2callbackapi',
    _credentials_class=CredentialsModelToDoApp,
    _credentials_property_name='api_credentials'
)

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)+'/templates/'),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class MainPageHandler(webapp2.RequestHandler):
    
    @decorator_app.auth_aware
    def get(self):
        if decorator_app.has_authentication():
            http = decorator_app.http()
            g_plus = build("plus", "v1", http=http) 
            mePerson = g_plus.people().get(userId='me').execute(http=http)

            template = JINJA_ENVIRONMENT.get_template('index.html')
            self.response.write(template.render({"user" : mePerson['displayName'], "logout_url" : users.create_logout_url('/')}))
        else:
            template = JINJA_ENVIRONMENT.get_template('login.html')
            self.response.write(template.render({"url" : decorator_app.authenticate_url()}))


class ApiHandler(webapp2.RequestHandler):

    def get(self):
        self.redirect('/')

    @decorator_api.oauth_aware    
    def head(self):
        if decorator_api.has_credentials():
            self.response.write('OK')
        else:
            self.response.write("Bad")
        
    def response_no_auth(self, server):
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(server.error(None, 401, "Not Authorized", {'redirect' : decorator_api.authorize_url()})))


    @decorator_api.oauth_aware    
    def post(self):
        server = jsonrpc.Server(self)
        if decorator_api.has_credentials():
            try:
                http = decorator_api.http()
                api_root = 'https://agh-gae-todo.appspot.com/_ah/api'
                api = 'todotaskendpoint'
                version = 'v1'
                discovery_url = '%s/discovery/v1/apis/%s/%s/rest' % (api_root, api, version)
                self.service = build(api, version, discoveryServiceUrl=discovery_url, http=http)
                server.handle(self.request, self.response)
            except AccessTokenRefreshError:
                self.response_no_auth(server)
        else:
            self.response_no_auth(server)

    def list_items(self, limit, cursor, filterDate):
        logging.info("Cursor: %s", cursor)
        params = {}
        params['limit'] = limit
        if cursor != "None":
            params['cursor'] = cursor
        if filterDate != 'None':
            params['lastUpdate'] = filterDate
        response = self.service.listToDoTask(**params).execute()
        return response

    def save_item(self, taskid, name, note, duedate, starred, status):

         
        body = {"name" : name, "note" : note, "dueDate": duedate, "starred" : starred, "status" : status}
        logging.info("Body: %s", body)
        if taskid=='0':
            response = self.service.insertToDoTask(body=body).execute()
        else:
            body["id"] = taskid
            response = self.service.updateToDoTask(body=body).execute()
        return response
            
