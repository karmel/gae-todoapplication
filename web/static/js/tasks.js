function displaySearchResultClear(response) {
    element = document.getElementById("content");
    element.innerHTML = '';
    displaySearchResult(response);
}

function displaySearchResult(response) {
        console.log(response.result);
        if (response.result) {
            var cursor = document.getElementById("cursor");
            cursor.value = response.result.nextPageToken;
            element = document.getElementById("content");
            response.result.items.forEach(function(entry) {
            var starred='<span class="badge">★</span>'
            var dueDate = new Date(entry.dueDate).toDateString();
            
            if(entry.starred)
                starred='<span class="badge yellow">★</span>'
            element.innerHTML += "<a id='formA' href='#' class='list-group-item' onClick='activateForm(this,true)'><span id='taskId'>"+entry.id+"</span>"+starred+"<h4 id='name' class='list-group-item-heading'>"+entry.name + "</h4><p id='note' class='list-group-item-text'>"+entry.note+"</p>Due: <span id='dueDate'>"+dueDate+"</span></a>";
            });
        } else if (response.error) {
            if(response.error.code == 401) {
                top.location.href=response.error.data.redirect;
            } else {
                getTasks(true);
            }
        }
                
};

function makeRequest(method, params, callback, id) {
var url = "/api";

var request = {};
request.method = method;
request.params = params;
request.jsonrpc = "2.0";
request.id = 1;
$.post(url, JSON.stringify(request), callback, "json");
}

function getTasks(clear) {
    var filterDate = document.getElementById("filterDate").value;
    var params = {}
    params.limit=5;
    params.cursor=document.getElementById("cursor").value;
    params.filterDate = "None";
    if(filterDate!="") {
        var date = new Date(filterDate);
        params.filterDate = date.toJSON();
    }
    if(clear) {
        params.cursor="None";
        makeRequest("list_items", params, displaySearchResultClear, 1);
    } else {
        makeRequest("list_items", params, displaySearchResult, 1);
    }
}

function reloadOnSave(response) {
    location.reload();
}

function saveTask(elem, done) {
  var date = new Date(elem.dueDate.value);
  console.log(elem.starred.value);
  var params = {}
  if(done) {
    params.status = 1;
  } else {
    params.status = 0;
  }
  params.taskid = elem.taskId.value;
  params.name = elem.name.value;
  params.note = elem.note.value;
  params.duedate = date.toJSON();
  params.starred = elem.starred.checked;
  
  makeRequest("save_item", params, reloadOnSave, 2);
}

function activateForm(box, done) {

    form = document.getElementById("realForm");
    if(box.id=='formA') {        
        form.taskId.value = $(box).children("#taskId")[0].innerHTML;
        form.name.value = $(box).children("#name")[0].innerHTML;
        form.note.value = $(box).children("#note")[0].innerHTML;
        form.dueDate.value = $(box).children("#dueDate")[0].innerHTML;
        var starred = typeof $(box).children(".yellow")[0];
        if(starred === "undefined") {
            form.starred.checked = false;
        } else {
            form.starred.checked = true;
        }
    } else {
        form.name.value = '' 
        form.note.value = '' 
        form.dueDate.value = '' 
        form.starred.checked = false;
        form.taskId.value = 0;
    }
    
    var elem = document.getElementById('done_button')
    if(done) {
        elem.style.display = "inline";
    } else {
        elem.style.display = 'none';
    }
    var it = document.getElementsByClassName('formHidden');
    Array.prototype.forEach.call(it, function(elem) {
        elem.style.display = "block";
    });
}

function deactivateForm() {
    var it = document.getElementsByClassName('formHidden');
    Array.prototype.forEach.call(it, function(elem) {
        elem.style.display = "none";
    });
}
