//
//  LoginViewController.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 20/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import "LoginViewController.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTLTodotaskendpointConstants.h"
#import "AppDelegate.h"
#import "CommunicationManager.h"

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)buttonClicked:(id)sender {

    [[CommunicationManager sharedInstance] authorizeFromViewController:self withDelegate:self];
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error {
    [self dismissModalViewControllerAnimated:YES];

    if (error != nil) {
        // Authentication failed
    } else {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self presentViewController:appDelegate.mainViewController animated:YES completion:nil];
    }
}

@end
