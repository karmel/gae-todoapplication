//
//  AppDelegate.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 09/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIStoryboard *mainStoryboard;

@property (strong, nonatomic) UIViewController *mainViewController;

@property (strong, nonatomic) UIViewController *loginViewController;

- (void)presentLoginView;

@end
