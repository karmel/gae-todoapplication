//
//  TaskTableViewCell.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 13/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SWTableViewCell.h>

@interface TaskTableViewCell : SWTableViewCell

@end
