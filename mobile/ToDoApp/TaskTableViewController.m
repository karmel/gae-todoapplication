//
//  TaskTableViewController.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 09/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaskTableViewController.h"
#import <NSMutableArray+SWUtilityButtons.h>
#import "TaskTableViewCell.h"
#import <AudioToolbox/AudioServices.h>

@interface TaskTableViewController () {
    NSMutableArray *data;
}
@end

@implementation TaskTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    return self;
}

- (void)loadData {

    FilterType filter = [self.tabBarController.viewControllers indexOfObject:self.navigationController]; // self.headerView.filterType;
    NSDate *d = self.headerView.dailyDate;

    switch (filter) {
    case DAILY:
        data = [[[CommunicationManager sharedInstance] getTasksForDay:d] mutableCopy];
        break;
    case MONTHLY:
        data = [[[CommunicationManager sharedInstance] getTasksForMonth:d] mutableCopy];
        break;
    case FUNCTIONAL:
        data = [[[CommunicationManager sharedInstance] getStarredTasks] mutableCopy];
        break;
    case ALL:
        data = [[[CommunicationManager sharedInstance] getAllTasks] mutableCopy];
        break;

    default:
        break;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor blueColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];

    [self.tableView registerNib:[UINib nibWithNibName:@"TaskTableViewCell" bundle:nil] forCellReuseIdentifier:@"TaskTableViewCell"];

    [self.tableView setBackgroundColor:[UIColor lightGrayColor]];

    NSUInteger currentTab = [self.tabBarController.viewControllers indexOfObject:self.navigationController];

    [self.headerView setFilterType:currentTab];

    if (currentTab <= 1) {
        if (!self.headerView) {
            self.headerView = [[[NSBundle mainBundle] loadNibNamed:@"TaskListHeader" owner:self options:nil] lastObject];
            [self.headerView setDelegate:self];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0;
}

- (void)viewWillAppear:(BOOL)animated {
    [self loadData];
    [super viewWillAppear:animated];
}

- (void)refreshData {
    [[CommunicationManager sharedInstance] getTasksWithDelegate:self];

    if (self.refreshControl) {

        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        [self.refreshControl endRefreshing];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSArray *)fetchData:(NSData *)response {

    return nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (data.count != 0) {
        self.tableView.backgroundView = nil;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        return 1;
    } else {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];

        messageLabel.text = @"No data is currently available.\nPlease pull down to refresh.";
        messageLabel.textColor = [UIColor grayColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Arial-Bold" size:6.0];
        [messageLabel sizeToFit];

        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"TaskTableViewCell";
    TaskTableViewCell *cell = [[TaskTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];

    cell.delegate = self;
    Task *t = (Task *)[data objectAtIndex:indexPath.row];
    [cell.detailTextLabel setText:t.desc];
    [cell.textLabel setText:t.name];

    cell.leftUtilityButtons = [self leftButtons];

    UIImage *accessoryImage = nil;
    if ([t.starred intValue] == 0) {
        accessoryImage = [UIImage imageNamed:@"starred_off.png"];

    } else {
        accessoryImage = [UIImage imageNamed:@"starred_on.png"];
    }
    UIImageView *accImageView = [[UIImageView alloc] initWithImage:accessoryImage];
    accImageView.userInteractionEnabled = YES;
    [accImageView setFrame:CGRectMake(0, 0, 28.0, 28.0)];
    cell.accessoryView = accImageView;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (!self.headerView) {
        return 0;
    }
    return 34.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    return self.headerView;
}

- (NSArray *)leftButtons {
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];

    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0] icon:[UIImage imageNamed:@"left.png"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188f alpha:1.0] icon:[UIImage imageNamed:@"left.png"]];
    return leftUtilityButtons;
}

#pragma mark SWTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    int row = [self.tableView indexPathForCell:cell].row;
    Task *t = (Task *)[data objectAtIndex:row];

    switch (index) {
    case FINISH:
        [self playSound];
        [t setStatus:[NSNumber numberWithInt:1]];
        break;
    case REMOVE:
        [t setStatus:[NSNumber numberWithInt:2]];
        break;
    default:
        break;
    }
    [[CommunicationManager sharedInstance] updateTask:t withDelegate:self];
    [cell hideUtilityButtonsAnimated:YES];
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return true;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    int row = indexPath.row;
    Task *t = (Task *)[data objectAtIndex:row];

    if ([t.starred intValue] == 1) {
        t.starred = [NSNumber numberWithInt:0];
    } else {
        t.starred = [NSNumber numberWithInt:1];
    }

    [[CommunicationManager sharedInstance] updateTask:t withDelegate:self];
}

#pragma mark Utils

- (void)playSound {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

#pragma mark GetTaskDelegate

- (void)onTasksReceived {
    [self loadData];
    [self.tableView reloadData];
}

#pragma mark EditTaskDelegate

- (void)onTaskModified {
    [self loadData];
    [self.tableView reloadData];
}

#pragma mark FilterDelegate

- (void)onFilterChanged {
    NSLog(@"Reloading data due to filter change");
    [self loadData];
    [self.tableView reloadData];
}

@end