//
//  ApplicationTableViewController.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 10/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import "ApplicationTableViewController.h"

@implementation ApplicationTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.102 green:0.506 blue:0.655 alpha:1];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
        NSForegroundColorAttributeName : [UIColor whiteColor],
        NSFontAttributeName : [UIFont fontWithName:@"Arial-Bold" size:0.0]
    }];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    self.navigationController.navigationBar.translucent = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
