//
//  MainTabBarController.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 09/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import "MainTabBarController.h"
#import "TaskTableViewController.h"
#import "TaskListHeader.h"
#import "AppDelegate.h"

@implementation MainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.102 green:0.506 blue:0.655 alpha:1]];
    self.delegate = self;
    [self selectDailyIcon];
}

- (void)selectDailyIcon {

    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    int weekday = [comps weekday];

    NSString *filename;
    switch (weekday) {
    case 1:
        filename = @"sunday";
        break;
    case 2:
        filename = @"monday";
        break;
    case 3:
        filename = @"tuesday";
        break;
    case 4:
        filename = @"wednesday";
        break;
    case 5:
        filename = @"thursday";
        break;
    case 6:
        filename = @"friday";
        break;
    case 7:
        filename = @"saturday";
        break;

    default:
        break;
    }

    UIViewController *vc = (UIViewController *)[self.viewControllers objectAtIndex:0];
    vc.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Daily" image:[UIImage imageNamed:filename] selectedImage:[UIImage imageNamed:filename]];
}

#pragma UITabBarDelegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
}

@end
