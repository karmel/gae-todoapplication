//
//  CommunicationManager.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 20/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GTMOAuth2Authentication.h"
#import <UIKit/UIKit.h>
#import "Task.h"

@protocol AddTaskDelegate <NSObject>

- (void)onTaskAdded;

@end

@protocol EditTaskDelegate <NSObject>

- (void)onTaskModified;

@end

@protocol GetTaskDelegate <NSObject>

- (void)onTasksReceived;

@end

@interface CommunicationManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) GTMOAuth2Authentication *authorization;

+ (CommunicationManager *)sharedInstance;

- (void)authorizeFromViewController:(UIViewController *)viewController withDelegate:(id)delegate;

- (bool)authorizeWithKeychain;

- (void)getTasksWithDelegate:(id<GetTaskDelegate>)delegate;

- (void)updateTask:(Task *)task withDelegate:(id<EditTaskDelegate>)delegate;

- (void)createTaskWithName:(NSString *)name andDesc:(NSString *)desc andDueDate:(NSDate *)date withDelegate:(id<AddTaskDelegate>)delegate;

- (void)logout;

- (NSArray *)getTasksForDay:(NSDate *)day;

- (NSArray *)getTasksForMonth:(NSDate *)month;

- (NSArray *)getStarredTasks;

- (NSArray *)getAllTasks;

@end
