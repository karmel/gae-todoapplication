//
//  TaskTableViewController.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 09/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationTableViewController.h"
#import <SWTableViewCell.h>
#import <SWTableViewCell.h>
#import "CommunicationManager.h"
#import "TaskListHeader.h"
#import "Task.h"

@interface TaskTableViewController : ApplicationTableViewController <SWTableViewCellDelegate, GetTaskDelegate, EditTaskDelegate>

typedef enum actionTypes { FINISH, REMOVE } ActionType;

@property (nonatomic, strong) TaskListHeader *headerView;

@end
