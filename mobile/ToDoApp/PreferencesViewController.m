//
//  PreferencesViewController.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 03/01/15.
//  Copyright (c) 2015 Mateusz Matrejek. All rights reserved.
//

#import "PreferencesViewController.h"
#import "TaskTableViewCell.h"
#import "TaskListHeader.h"
#import "CommunicationManager.h"

@implementation PreferencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setBackgroundColor:[UIColor lightGrayColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"PrefsViewCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

    switch (indexPath.section) {
    case 0:
        cell.textLabel.text = @"Sign me out";
        cell.textLabel.textColor = [UIColor redColor];
        cell.textLabel.textAlignment = UITextAlignmentCenter;
        break;

    case 1:
        cell.textLabel.text = @"Project site";
        break;

    default:
        break;
    }

    return cell;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 42.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 34.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    TaskListHeader *header = [[[NSBundle mainBundle] loadNibNamed:@"PreferencesHeader" owner:self options:nil] lastObject];
    [header setFilterType:UNDEFINED];
    switch (section) {
    case 0:
        [header.label setText:@"Account settings"];
        break;
    case 1:
        [header.label setText:@"About"];
        break;
    default:
        break;
    }

    return header;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [[CommunicationManager sharedInstance] logout];
        return;
    }

    if (indexPath.section == 1) {
        NSURL *url = [NSURL URLWithString:@"https://bitbucket.org/karmel/gae-todoapplication/"];
        if (![[UIApplication sharedApplication] openURL:url]) {
            NSLog(@"%@%@", @"Failed to open url:", [url description]);
        }
    }
}

@end
