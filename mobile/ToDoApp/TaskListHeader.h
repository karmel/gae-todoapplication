//
//  TaskListHeader.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 13/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol FilterDelegate <NSObject>

- (void)onFilterChanged;

@end

@interface TaskListHeader : UITableViewCell

typedef enum filterTypes { DAILY, MONTHLY, FUNCTIONAL, ALL, UNDEFINED } FilterType;

typedef enum filterChanges { PREV, NEXT } FilterChange;

@property (atomic) int filterType;

@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UIButton *leftButton;

@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@property (strong, nonatomic) NSDate *dailyDate;

@property (strong, nonatomic) NSDateFormatter *dailyFormatter;

@property (strong, nonatomic) NSDateFormatter *monthlyFormatter;

@property (nonatomic) id<FilterDelegate> delegate;

- (void)updateLabel;

@end
