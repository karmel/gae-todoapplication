//
//  Task.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 21/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GTLTodotaskendpointToDoTask.h"

@interface Task : NSManagedObject

- (id)initWithApiObject:(GTLTodotaskendpointToDoTask *)task;

- (GTLTodotaskendpointToDoTask *)convertToApiObject;

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSDate *dueDate;
@property (nonatomic, retain) NSNumber *starred;
@property (nonatomic, retain) NSNumber *taskId;
@property (nonatomic, retain) NSNumber *status;

@end
