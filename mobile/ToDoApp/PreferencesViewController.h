//
//  PreferencesViewController.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 03/01/15.
//  Copyright (c) 2015 Mateusz Matrejek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApplicationTableViewController.h"

@interface PreferencesViewController : ApplicationTableViewController

@end
