//
//  CommunicationManager.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 20/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import "CommunicationManager.h"
#import "GTLServiceTodotaskendpoint.h"
#import "GTMHTTPFetcher.h"
#import "GTLQueryTodotaskendpoint.h"
#import "GTLTodotaskendpointToDoTask.h"
#import "GTMOAuth2Authentication.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTLTodotaskendpointConstants.h"
#import "Task.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "GTLTodotaskendpointCollectionResponseToDoTask.h"

@implementation CommunicationManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static GTLServiceTodotaskendpoint *service = nil;

static NSString *const kKeychainItemName = @"ToDoApp";
static NSString *const kMyClientID = @"606981375120-g890je64k0c9ov7pog4s35bs1hnp3463.apps.googleusercontent.com";
static NSString *const kMyClientSecret = @"gaaymc1EZiqetOlJKRiIFZY0";

+ (CommunicationManager *)sharedInstance {
    static CommunicationManager *sharedInstance = nil;
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[CommunicationManager alloc] init];
        }
    };
    return sharedInstance;
}

- (void)logout {

    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];

    if (self.authorization) {
        [GTMOAuth2ViewControllerTouch revokeTokenForGoogleAuthentication:self.authorization];
    }

    [self removeAllTasks];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate presentLoginView];
}

- (id)init {
    self = [super init];
    if (self) {

        if (!service) {
            service = [[GTLServiceTodotaskendpoint alloc] init];
            service.retryEnabled = YES;
        }
    }
    return self;
}

- (void)getTasksWithDelegate:(id<GetTaskDelegate>)delegate {

    GTLQueryTodotaskendpoint *getTaskQuery = [GTLQueryTodotaskendpoint queryForListToDoTask];
    [getTaskQuery setLastUpdate:nil];

    [service executeQuery:getTaskQuery
        completionHandler:^(GTLServiceTicket *ticket, GTLTodotaskendpointCollectionResponseToDoTask *object, NSError *error) {

            if (error != nil) {
                NSLog(@"Error while updating task %@", error.localizedDescription);
                return;
            }

            for (int i = 0; i < [object items].count; i++) {
                GTLTodotaskendpointToDoTask *task = (GTLTodotaskendpointToDoTask *)[object itemAtIndex:i];
                [self insertOrUpdateTask:task];
            }
            [delegate onTasksReceived];
        }];
}

- (void)updateTask:(Task *)task withDelegate:(id<EditTaskDelegate>)delegate {
    GTLTodotaskendpointToDoTask *t = [task convertToApiObject];

    GTLQueryTodotaskendpoint *updateTaskQuery = [GTLQueryTodotaskendpoint queryForUpdateToDoTaskWithObject:t];

    [service executeQuery:updateTaskQuery
        completionHandler:^(GTLServiceTicket *ticket, GTLTodotaskendpointToDoTask *object, NSError *error) {

            if (error != nil) {
                NSLog(@"Error while updating task %@", error.localizedDescription);
                return;
            }

            [self insertOrUpdateTask:object];
            [delegate onTaskModified];
        }];
}

- (void)createTaskWithName:(NSString *)name andDesc:(NSString *)desc andDueDate:(NSDate *)date withDelegate:(id<AddTaskDelegate>)delegate {

    GTLTodotaskendpointToDoTask *t = [[GTLTodotaskendpointToDoTask alloc] init];
    [t setName:name];
    [t setNote:desc];
    [t setStarred:[NSNumber numberWithInt:0]];
    [t setDueDate:[GTLDateTime dateTimeForAllDayWithDate:date]];

    GTLQueryTodotaskendpoint *updateTaskQuery = [GTLQueryTodotaskendpoint queryForInsertToDoTaskWithObject:t];

    [service executeQuery:updateTaskQuery
        completionHandler:^(GTLServiceTicket *ticket, GTLTodotaskendpointToDoTask *object, NSError *error) {

            if (error != nil) {
                NSLog(@"Error while creating task %@", error.localizedDescription);
                return;
            }

            [self insertOrUpdateTask:object];
            [delegate onTaskAdded];
        }];
}

- (bool)authorizeWithKeychain {

    self.authorization = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName clientID:kMyClientID clientSecret:kMyClientSecret];

    if ([self.authorization canAuthorize]) {
        [service setAuthorizer:self.authorization];
        return true;
    }

    return false;
}

- (void)authorizeFromViewController:(UIViewController *)viewController withDelegate:(id)delegate {

    GTMOAuth2ViewControllerTouch *authController;
    authController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:kGTLAuthScopeTodotaskendpointUserinfoEmail
                                                                clientID:kMyClientID
                                                            clientSecret:kMyClientSecret
                                                        keychainItemName:kKeychainItemName
                                                                delegate:delegate
                                                        finishedSelector:@selector(viewController:finishedWithAuth:error:)];

    [viewController presentViewController:authController animated:YES completion:nil];
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    NSURL *storeUrl = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ToDoApp.sqlite"];

    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _persistentStoreCoordinator;
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ToDoApp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSArray *)getTasksForDay:(NSDate *)day {
    NSPredicate *p = [NSPredicate predicateWithFormat:@"status == 0"];
    return [self getTasksWithPredicate:p];
}

- (NSArray *)getTasksForMonth:(NSDate *)month {
    NSPredicate *p = [NSPredicate predicateWithFormat:@"status == 0"];
    return [self getTasksWithPredicate:p];
}

- (NSArray *)getStarredTasks {
    NSPredicate *p = [NSPredicate predicateWithFormat:@"status == 0 and starred == 1"];
    return [self getTasksWithPredicate:p];
}

- (NSArray *)getAllTasks {
    NSPredicate *p = [NSPredicate predicateWithFormat:@"status == 0"];
    return [self getTasksWithPredicate:p];
}

- (Task *)getTaskWithId:(NSNumber *)taskId {

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"taskId = %@", taskId];
    NSArray *taskArray = [self getTasksWithPredicate:predicate];

    if (taskArray.count > 0) {
        return [taskArray objectAtIndex:0];
    }

    return nil;
}

- (NSArray *)getTasksWithPredicate:(NSPredicate *)filter {

    NSManagedObjectContext *moc = [self managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:moc];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];

    if (filter) {
        [request setPredicate:filter];
    }

    NSError *error;
    NSArray *array = [moc executeFetchRequest:request error:&error];
    if (array == nil) {
        NSLog(@"Error on getting tasks from db %@", error.description);
    }

    return array;
}

- (void)removeAllTasks {
    NSArray *tasks = [self getTasksWithPredicate:NULL];
    NSManagedObjectContext *context = [self managedObjectContext];

    for (Task *t in tasks) {
        [context deleteObject:t];
    }
}

- (void)insertOrUpdateTask:(GTLTodotaskendpointToDoTask *)task {

    NSManagedObjectContext *context = [self managedObjectContext];
    Task *t;
    t = [self getTaskWithId:task.identifier];

    if (t) {
        NSLog(@"Will update task...");
    } else {
        t = [NSEntityDescription insertNewObjectForEntityForName:@"Task" inManagedObjectContext:context];
    }
    t.taskId = task.identifier;
    t.name = task.name;
    t.starred = task.starred;
    t.desc = task.note;
    t.status = task.status;
    t.dueDate = task.dueDate.date;

    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Could not save: %@", [error localizedDescription]);
    } else {
        NSLog(@"Saving...");
    }
}

@end
