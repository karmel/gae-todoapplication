//
//  AddTaskTableViewController.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 10/01/15.
//  Copyright (c) 2015 Mateusz Matrejek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommunicationManager.h"
#import "ApplicationTableViewController.h"

@interface AddTaskTableViewController : ApplicationTableViewController <UITextFieldDelegate, AddTaskDelegate>

@end
