//
//  AddTaskTableViewController.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 10/01/15.
//  Copyright (c) 2015 Mateusz Matrejek. All rights reserved.
//

#import "AddTaskTableViewController.h"
#import <UIKit/UIKit.h>
#import "Task.h"
#import "CommunicationManager.h"

#define kPickerAnimationDuration 0.40 // duration for the animation to slide the date picker into view
#define kDatePickerTag 99             // view tag identifiying the date picker view

#define kTitleKey @"title" // key for obtaining the data source item's title
// key for obtaining the data source item's date value
#define kDateStartRow 1

static NSString *kDateCellID = @"dateCell";     // the cells with the start or end date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kOtherCell = @"otherCell";     // the remaining cells at the end

#pragma mark -

@interface AddTaskTableViewController ()

@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;
@property (assign) NSInteger pickerCellRowHeight;

@property (nonatomic, strong) IBOutlet UIDatePicker *pickerView;

@property (nonatomic, strong) NSDate *dueDate;
@property (nonatomic, strong) UITextField *nameView;
@property (nonatomic, strong) UITextField *descriptionView;

@end

@implementation AddTaskTableViewController

#pragma mark - Table view data source

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    NSMutableDictionary *itemOne = [@{ kTitleKey : @"Name" } mutableCopy];
    NSMutableDictionary *itemTwo = [@{ kTitleKey : @"Note" } mutableCopy];
    NSMutableDictionary *itemThree = [@{ kTitleKey : @"Due date" } mutableCopy];
    NSMutableDictionary *itemFour = [@{ kTitleKey : @"Create!" } mutableCopy];

    self.dataArray = @[ itemOne, itemThree, itemTwo, itemFour ];

    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    UITableViewCell *pickerViewCellToCheck = [self.tableView dequeueReusableCellWithIdentifier:kDatePickerID];
    self.pickerCellRowHeight = CGRectGetHeight(pickerViewCellToCheck.frame);

    self.nameView = [[UITextField alloc] initWithFrame:CGRectMake(110, 10, 185, 30)];
    self.nameView.adjustsFontSizeToFitWidth = YES;
    self.nameView.textColor = [UIColor blackColor];
    self.nameView.placeholder = @"Task name";
    [self.nameView setDelegate:self];

    self.descriptionView = [[UITextField alloc] initWithFrame:CGRectMake(110, 10, 185, 30)];
    self.descriptionView.adjustsFontSizeToFitWidth = YES;
    self.descriptionView.textColor = [UIColor blackColor];
    self.descriptionView.placeholder = @"Task description";
    [self.descriptionView setDelegate:self];

    self.dueDate = [[NSDate alloc] init];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeChanged:) name:NSCurrentLocaleDidChangeNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSCurrentLocaleDidChangeNotification object:nil];
}

#pragma mark - Utils

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }

    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }

    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)localeChanged:(NSNotification *)notif {
    [self.tableView reloadData];
}

- (BOOL)hasPickerForIndexPath:(NSIndexPath *)indexPath {
    BOOL hasDatePicker = NO;

    NSInteger targetedRow = indexPath.row;
    targetedRow++;

    UITableViewCell *checkDatePickerCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:targetedRow inSection:0]];
    UIDatePicker *checkDatePicker = (UIDatePicker *)[checkDatePickerCell viewWithTag:kDatePickerTag];

    hasDatePicker = (checkDatePicker != nil);
    return hasDatePicker;
}

- (void)updateDatePicker {
    if (self.datePickerIndexPath != nil) {
        UITableViewCell *associatedDatePickerCell = [self.tableView cellForRowAtIndexPath:self.datePickerIndexPath];

        UIDatePicker *targetedDatePicker = (UIDatePicker *)[associatedDatePickerCell viewWithTag:kDatePickerTag];
        if (targetedDatePicker != nil) {
            NSDictionary *itemData = self.dataArray[self.datePickerIndexPath.row - 1];
            [targetedDatePicker setDate:self.dueDate animated:NO];
        }
    }
}

- (BOOL)hasInlineDatePicker {
    return (self.datePickerIndexPath != nil);
}

- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath {
    return ([self hasInlineDatePicker] && self.datePickerIndexPath.row == indexPath.row);
}

- (BOOL)indexPathHasDate:(NSIndexPath *)indexPath {
    if (indexPath.row == kDateStartRow) {
        return YES;
    }
    return NO;
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ([self indexPathHasPicker:indexPath] ? self.pickerCellRowHeight : self.tableView.rowHeight);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self hasInlineDatePicker]) {
        // we have a date picker, so allow for it in the number of rows in this section
        NSInteger numRows = self.dataArray.count;
        return ++numRows;
    }

    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;

    NSString *cellID = kOtherCell;

    if ([self indexPathHasPicker:indexPath]) {
        cellID = kDatePickerID;
    } else if ([self indexPathHasDate:indexPath]) {
        cellID = kDateCellID;
    }

    cell = [tableView dequeueReusableCellWithIdentifier:cellID];

    NSInteger modelRow = indexPath.row;
    if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row <= indexPath.row) {
        modelRow--;
    }

    NSDictionary *itemData = self.dataArray[modelRow];

    if ([cellID isEqualToString:kDateCellID]) {
        cell.textLabel.text = [itemData valueForKey:kTitleKey];
        cell.detailTextLabel.text = [self.dateFormatter stringFromDate:self.dueDate];
    } else if ([cellID isEqualToString:kOtherCell]) {
        NSLog(@"%d", indexPath.row);
        if (indexPath.row == 0) {
            [cell.contentView addSubview:self.nameView];
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else if (indexPath.row == 2) {
            [cell.contentView addSubview:self.descriptionView];
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else if (indexPath.row == 3) {
            cell.textLabel.textAlignment = UITextAlignmentCenter;
            cell.textLabel.textColor = [UIColor greenColor];
        }
        cell.textLabel.text = [itemData valueForKey:kTitleKey];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)toggleDatePickerForSelectedIndexPath:(NSIndexPath *)indexPath {
    [self.tableView beginUpdates];

    NSArray *indexPaths = @[ [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0] ];

    if ([self hasPickerForIndexPath:indexPath]) {
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    } else {
        [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    }

    [self.tableView endUpdates];
}

- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView beginUpdates];

    BOOL before = NO; // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineDatePicker]) {
        before = self.datePickerIndexPath.row < indexPath.row;
    }

    BOOL sameCellClicked = (self.datePickerIndexPath.row - 1 == indexPath.row);

    if ([self hasInlineDatePicker]) {
        [self.tableView deleteRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0] ]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.datePickerIndexPath = nil;
    }

    if (!sameCellClicked) {
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];

        [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
        self.datePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }

    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.tableView endUpdates];
    [self updateDatePicker];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];

    if ([[c textLabel].text isEqualToString:@"Create!"]) {
        NSLog(@"chuuj!");
        [self addTask];
        return;
    }

    if (cell.reuseIdentifier == kDateCellID) {
        [self displayInlineDatePickerForRowAtIndexPath:indexPath];
    } else {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark - Actions

- (IBAction)dateAction:(id)sender {
    NSIndexPath *targetedCellIndexPath = nil;

    if ([self hasInlineDatePicker]) {
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.datePickerIndexPath.row - 1 inSection:0];
    } else {
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }

    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    UIDatePicker *targetedDatePicker = sender;

    NSMutableDictionary *itemData = self.dataArray[targetedCellIndexPath.row];
    self.dueDate = targetedDatePicker.date;
    cell.detailTextLabel.text = [self.dateFormatter stringFromDate:targetedDatePicker.date];
}

- (void)addTask {

    NSString *name = self.nameView.text;
    NSString *desc = self.descriptionView.text;
    NSDate *dueDate = self.dueDate;

    bool validationFailed = false;

    if ((name == NULL) || ([name isEqualToString:@""])) {
        [self.nameView setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.textColor"];
        [self.nameView setPlaceholder:@"Name can't be empty"];
        validationFailed = true;
    }

    if ((desc == NULL) || ([desc isEqualToString:@""])) {
        [self.descriptionView setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.textColor"];
        [self.descriptionView setPlaceholder:@"Note can't be empty"];
        validationFailed = true;
    }

    if (validationFailed) {
        return;
    }

    [[CommunicationManager sharedInstance] createTaskWithName:name andDesc:desc andDueDate:dueDate withDelegate:self];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark AddTaskDelegate

- (void)onTaskAdded {
    NSLog(@"task added");
    [self.navigationController popViewControllerAnimated:YES];
}

@end
