//
//  TaskListHeader.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 13/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import "TaskListHeader.h"

@implementation TaskListHeader

- (void)layoutSubviews {
    [super layoutSubviews];
    self.dailyDate = [NSDate date];
    self.dailyFormatter = [[NSDateFormatter alloc] init];
    [self.dailyFormatter setDateFormat:@"dd MMMM yyyy"];
    self.monthlyFormatter = [[NSDateFormatter alloc] init];
    [self.monthlyFormatter setDateFormat:@"MMMM yyyy"];

    if (self.filterType != UNDEFINED) {
        [self updateLabel];
    }
}

- (IBAction)leftButtonClicked:(id)sender {
    [self updateViewWithChange:PREV];
}

- (IBAction)rightButtonClicked:(id)sender {
    [self updateViewWithChange:NEXT];
}

- (void)updateViewWithChange:(FilterChange)change {

    NSCalendar *calendar = [NSCalendar currentCalendar];

    switch ([self filterType]) {
    case DAILY:

        if (change == NEXT) {
            self.dailyDate = [self.dailyDate dateByAddingTimeInterval:60 * 60 * 24 * 1];
        } else {
            self.dailyDate = [self.dailyDate dateByAddingTimeInterval:-60 * 60 * 24 * 1];
        }

        break;
    case MONTHLY:
        if (change == NEXT) {
            NSDateComponents *components = [[NSDateComponents alloc] init];
            [components setMonth:1];
            self.dailyDate = [calendar dateByAddingComponents:components toDate:self.dailyDate options:0];

        } else {
            NSDateComponents *components = [[NSDateComponents alloc] init];
            [components setMonth:-1];
            self.dailyDate = [calendar dateByAddingComponents:components toDate:self.dailyDate options:0];
        }

        break;

    default:
        break;
    }
    [self updateLabel];

    if (self.delegate) {
        [self.delegate onFilterChanged];
    }
}

- (void)updateLabel {

    switch ([self filterType]) {
    case DAILY:
        [self.label setText:[self.dailyFormatter stringFromDate:self.dailyDate]];
        break;
    case MONTHLY:
        [self.label setText:[self.monthlyFormatter stringFromDate:self.dailyDate]];
        break;

    default:
        break;
    }
}

@end
