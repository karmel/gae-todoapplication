//
//  Task.m
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 21/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import "Task.h"

@implementation Task

@dynamic name;
@dynamic desc;
@dynamic status;
@dynamic dueDate;
@dynamic starred;
@dynamic taskId;

- (id)initWithApiObject:(GTLTodotaskendpointToDoTask *)task {
    self = [super init];
    if (self) {
        // self.taskId = task.identifier;
        self.name = task.name;
        self.desc = task.note;
        self.status = task.status;
        self.starred = task.starred;
        self.dueDate = task.dueDate.date;
    }
    return self;
}

- (GTLTodotaskendpointToDoTask *)convertToApiObject {

    GTLTodotaskendpointToDoTask *t = [[GTLTodotaskendpointToDoTask alloc] init];
    t.identifier = self.taskId;
    t.name = self.name;
    t.starred = self.starred;
    t.note = self.desc;
    t.status = self.status;
    t.dueDate = [GTLDateTime dateTimeForAllDayWithDate:self.dueDate];

    return t;
}

@end
