//
//  PreferencesTableViewCell.h
//  ToDoApp
//
//  Created by Matrejek, Mateusz on 10/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PreferencesTableViewCell : UITableViewCell

@end
